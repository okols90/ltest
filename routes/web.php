<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth'])->group(function () {
    Route::resource('users', 'UsersController')->except('show');
    Route::resource('department', 'DepartmentsController')->except('show');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
