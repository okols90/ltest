@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Departments') }}  <a href="{{ route('department.create') }}" class="btn btn-secondary btn-sm float-right">{{ __('Add') }}</a></div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">{{ __('Logo') }}</th>
                                <th scope="col">{{ __('Text') }}</th>
                                <th scope="col">{{ __('Users') }}</th>
                                <th scope="col">{{ __('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($departments as $department)
                                <tr>
                                    <td><img src="{{ asset($department->logo) }}" style="max-width: 100px; height: auto"></td>
                                    <td>
                                        {{ $department->name }}<br>
                                        {{ $department->description }}
                                    </td>
                                    <td>
                                        <ul>
                                            @foreach($department->users()->pluck('name') as $name)
                                                <li>{{ $name }}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    <td>
                                        <a href="{{ route('department.edit', $department->id) }}" class="btn btn-secondary btn-sm">{{ __('Edit') }}</a>
                                        <button type="submit" form="formDestroy" class="btn btn-danger btn-sm">{{ __('Delete') }}</button>
                                        <form action="{{ route('department.destroy', $department->id) }}" method="POST" id="formDestroy">
                                            @method('DELETE')
                                            @csrf
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{ $departments->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

