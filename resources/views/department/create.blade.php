@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Departments') }}</div>

                    <div class="card-body">
                        <form action="{{ route('department.store') }}" method="POST" class="w-100" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="inputName">{{ __('Name') }}</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" id="inputName" placeholder="Enter name" name="name" value="{{ old('name') }}">
                                @error('name')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="inputDescription">{{ __('Description') }}</label>
                                <textarea type="text" class="form-control @error('description') is-invalid @enderror" id="inputDescription" placeholder="Enter description" name="description">{{ old('description') }}</textarea>
                                @error('description')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="inputLogo">{{ __('Logo') }}</label>
                                <input type="file" class="form-control @error('logo') is-invalid @enderror" id="inputLogo" placeholder="Enter logo" accept="image/*" name="logo" value="{{ old('logo') }}">
                                @error('logo')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            @foreach($users as $user)
                                <div class="form-group" style="margin-bottom: 0">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox{{ $user->id }}" name="users[]">
                                        <label class="form-check-label" for="inlineCheckbox{{ $user->id }}">{{ $user->name }}</label>
                                    </div>
                                </div>
                            @endforeach

                            <button type="submit" class="btn btn-primary">Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

